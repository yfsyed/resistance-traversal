package com.alliance.assignment;

import com.alliance.assignment.listeners.OnResultsCallback;
import com.alliance.assignment.presenter.ResistanceResultPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class ResistanceResultPresenterTest {

    private String mGrid = TestData.TEST_GRID_STRING1;
    private String mResult = "YES";
    private int mResistance = 16;
    private String mPath = "[1, 2, 3, 4, 4, 5]";
    private ResistanceResultPresenter mPresenter;

    @Before
    public void setup() {
        mPresenter = new ResistanceResultPresenter();
    }

    @After
    public void tearDown() {
        mPresenter.removeListener();
        mPresenter = null;
    }

    @Test
    public void successfulResistanceResultsTest() {
        mPresenter.setListener(mPositiveCallback);
        mPresenter.onResistanceResults(mGrid,mResult,mResistance,mPath);
    }

    @Test
    public void failureResistanceResultsTest() {
        mPresenter.setListener(mNegativeCallback);
        mPresenter.onResistanceResults(TestData.TEST_GRID_STRING2,"NO",40,"[1,1,1,1]");
    }

    OnResultsCallback mPositiveCallback = new OnResultsCallback() {

        @Override
        public void updatePath(String path) {
            assertEquals("Path must be same", mPath, path);
        }

        @Override
        public void updateTotalResistance(int resistance) {
            assertEquals("Resistance must be same", mResistance, resistance);
        }

        @Override
        public void updateGrid(String grid) {
            assertEquals("Grid must be same", mGrid, grid);
        }

        @Override
        public void updateResult(String result) {
            assertEquals("Result must be same", mResult, result);
        }
    };

    OnResultsCallback mNegativeCallback = new OnResultsCallback() {

        @Override
        public void updatePath(String path) {
            assertNotEquals("Path must not be same", mPath, path);
        }

        @Override
        public void updateTotalResistance(int resistance) {
            assertNotEquals("Resistance must not be same", mResistance, resistance);
        }

        @Override
        public void updateGrid(String grid) {
            assertNotEquals("Grid must not be same", mGrid, grid);
        }

        @Override
        public void updateResult(String result) {
            assertNotEquals("Result must not be same", mResult, result);
        }
    };


}
