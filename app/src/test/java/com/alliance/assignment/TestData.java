package com.alliance.assignment;

public class TestData {

    public static final int INPUT_1[][] = {{3, 4, 1, 2, 8, 6},
            {6, 1, 8, 2, 7, 4}, {5, 9, 3, 9, 9, 5}, {8, 4, 1, 3, 2, 6},
            {3, 7, 2, 8, 6, 4}};

    public static final int WEIGHTED_GRID[][] = {
            {16, 15, 13, 12, 12, 6}, {19, 13, 20, 13, 11, 4},
            {18, 19, 12, 15, 13, 5}, {22, 14, 10, 9, 6, 6},
            {17, 17, 11, 14, 10, 4}};

    public static final int INPUT_2[][] = {{3, 4, 1, 2, 8, 6},
            {6, 1, 8, 2, 7, 4}, {5, 9, 3, 9, 9, 5}, {8, 4, 1, 3, 2, 6},
            {3, 7, 2, 1, 2, 3}};

    public static final int INPUT_3[][] = {{19, 10, 19, 10, 19},
            {21, 23, 20, 19, 12}, {20, 12, 20, 11, 10}};


    public static final String TEST_GRID_STRING1 = "" +
            "3 4 1 2 8 6 \n" +
            "6 1 8 2 7 4 \n" +
            "5 9 3 9 9 5 \n" +
            "8 4 1 3 2 6 \n" +
            "3 7 2 8 6 4 ";


    public static final String TEST_GRID_STRING2 = "" +
            "19 10 19 10 19 \n" +
            "21 23 20 19 12 \n" +
            "20 12 20 11 10 ";
}
