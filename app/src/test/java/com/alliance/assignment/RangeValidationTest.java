package com.alliance.assignment;

import com.alliance.assignment.model.Extras;
import com.alliance.assignment.utils.OutOfRangeException;
import com.alliance.assignment.utils.ResistanceTraversalHelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;


public class RangeValidationTest {

	@Test
	public void rowLowerRangeFailureTest() {
		String actual = "";
		try {
			ResistanceTraversalHelper.validateMatrixRange(-1, 5);
		} catch (OutOfRangeException ore) {
			actual = ore.getMessage();
		}
		assertEquals("unable to validate lower row range",
				Extras.ROW_RANGE_EXCEPTION, actual);
	}

	@Test
	public void rowHigherRangeFailureTest() {
		String actual = "";
		try {
			ResistanceTraversalHelper.validateMatrixRange(15, 50);
		} catch (OutOfRangeException ore) {
			actual = ore.getMessage();
		}
		assertEquals("unable to validate higher row range",
				Extras.ROW_RANGE_EXCEPTION, actual);
	}

	@Test
	public void rowLowerRangeSuccessTest() {
		try {
			ResistanceTraversalHelper.validateMatrixRange(2, 5);
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		}
	}

	@Test
	public void rowHigherRangeSuccessTest() {
		try {
			ResistanceTraversalHelper.validateMatrixRange(10, 50);
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		}
	}

	@Test
	public void columnLowerRangeFailureTest() {
		String actual = "";
		try {
			ResistanceTraversalHelper.validateMatrixRange(5, 3);
		} catch (OutOfRangeException ore) {
			actual = ore.getMessage();
		}
		assertEquals("unable to validate lower column range",
				Extras.COLUMN_RANGE_EXCEPTION, actual);
	}

	@Test
	public void columnHigherRangeFailureTest() {
		String actual = "";
		try {
			ResistanceTraversalHelper.validateMatrixRange(10, 110);
		} catch (OutOfRangeException ore) {
			actual = ore.getMessage();
		}
		assertEquals("unable to validate higher column range",
				Extras.COLUMN_RANGE_EXCEPTION, actual);
	}

	@Test
	public void columnLowerRangeSuccessTest() {
		try {
			ResistanceTraversalHelper.validateMatrixRange(1, 5);
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		}
	}

	@Test
	public void columnHigherRangeSuccessTest() {
		try {
			ResistanceTraversalHelper.validateMatrixRange(10, 100);
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		}
	}
	
	

}
