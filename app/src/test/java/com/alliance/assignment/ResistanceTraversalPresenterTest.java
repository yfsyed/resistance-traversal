package com.alliance.assignment;

import com.alliance.assignment.presenter.ResistanceTraversalPresenter;
import com.alliance.assignment.utils.OutOfRangeException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ResistanceTraversalPresenterTest {

    private ResistanceTraversalPresenter rt;

    @Before
    public void setUp() throws Exception {
        rt = new ResistanceTraversalPresenter();
    }

    @After
    public void tearDown() throws Exception {
        rt = null;
    }

    @Test
    public void evaluateLRPNullPointerTest() {
        boolean flag = false;
        try {
            rt.initResistanceMatrix(3, 6);
            rt.setResistance(null);
            rt.calculateWeightsToDestination();
            rt.evaluateResistance(1);
        } catch (OutOfRangeException ore) {
            fail(ore.getMessage());
        } catch (ArrayIndexOutOfBoundsException aobe) {
            fail(aobe.getMessage());
        } catch (NullPointerException npe) {
            flag = true;
        }
        assertTrue("NullPointerException expected", flag);
    }

    @Test
    public void evaluateLRPIndexOutofBoundTest() {
        boolean flag = false;
        try {
            rt.initResistanceMatrix(3, 6);
            rt.setResistance(TestData.INPUT_3);
            rt.calculateWeightsToDestination();
            rt.evaluateResistance(1);
        } catch (OutOfRangeException ore) {
            fail(ore.getMessage());
        } catch (ArrayIndexOutOfBoundsException aobe) {
            flag = true;
        } catch (NullPointerException npe) {
            fail(npe.getMessage());
        }
        assertTrue("ArrayIndexOutOfBoundsException expected", flag);
    }

    @Test
    public void evaluateLeastResistancePathFailureTest() {
        try {
            rt.initResistanceMatrix(3, 5);
            rt.setResistance(TestData.INPUT_3);
            rt.calculateWeightsToDestination();
            rt.evaluateResistance(1);
        } catch (OutOfRangeException ore) {
            fail(ore.getMessage());
        } catch (ArrayIndexOutOfBoundsException aobe) {
            fail(aobe.getMessage());
        } catch (NullPointerException npe) {
            fail(npe.getMessage());
        }
        assertEquals("Resistance path traversal unsuccessful", "NO", rt.getEnergyReleased());
    }

    @Test
    public void evaluateLeastResistancePathSuccessTest() {
        try {
            rt.initResistanceMatrix(5, 6);
            rt.setResistance(TestData.INPUT_1);
            rt.calculateWeightsToDestination();
            rt.evaluateResistance(1);
        } catch (OutOfRangeException ore) {
            fail(ore.getMessage());
        } catch (ArrayIndexOutOfBoundsException aobe) {
            fail(aobe.getMessage());
        } catch (NullPointerException npe) {
            fail(npe.getMessage());
        }

        assertEquals("Resistance path traversal unsuccessful", "YES", rt.getEnergyReleased());
    }

}
