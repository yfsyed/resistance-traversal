package com.alliance.assignment;

import com.alliance.assignment.listeners.RowColumnCallback;
import com.alliance.assignment.model.Extras;
import com.alliance.assignment.presenter.RowColumnInputPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class RowColumnInputPresenterTest {

    private RowColumnInputPresenter mPresenter;

    private int mRow, mColumns;

    private String errorMessage;

    @Before
    public void setup() {
        mPresenter = new RowColumnInputPresenter();
    }

    @After
    public void tearDown() {
        mPresenter.removeListener();
        mPresenter = null;
    }

    @Test
    public void successfulResistanceResultsTest() {
        mPresenter.setListener(mPositiveCallback);
        mRow = 5;
        mColumns = 6;
        mPresenter.sendRowColumn("" + 5, "" + 6);
    }

    @Test
    public void failureResistanceResultsRangeTest() {
        mPresenter.setListener(mNegativeCallback);
        mRow = 5;
        mColumns = 6;
        mPresenter.sendRowColumn("" + 4, "" + 8);
    }

    @Test
    public void successfulInvalidColumnTest() {
        mPresenter.setListener(mPositiveCallback);
        mRow = 5;
        mColumns = 2;
        errorMessage = Extras.COLUMN_RANGE_EXCEPTION;
        mPresenter.sendRowColumn("" + 4, "" + 2);
    }

    @Test
    public void successfulInvalidRowTest() {
        mPresenter.setListener(mPositiveCallback);
        mRow = 5;
        mColumns = 2;
        errorMessage = Extras.ROW_RANGE_EXCEPTION;
        mPresenter.sendRowColumn("" + 15, "" + 6);
    }

    @Test
    public void failureInvalidRowTest() {
        mPresenter.setListener(mNegativeCallback);
        mRow = 5;
        mColumns = 2;
        errorMessage = Extras.ROW_RANGE_EXCEPTION;
        mPresenter.sendRowColumn("" + 6, "" + 6);
    }

    RowColumnCallback mPositiveCallback = new RowColumnCallback() {

        @Override
        public void gotoFeedResistanceInput(int row, int column) {
            assertEquals("Rows should be same", mRow, row);
            assertEquals("Columns should be same", mColumns, column);
        }

        @Override
        public void onInvalidInput(String message) {
            assertEquals("Error message must be same", errorMessage, message);
        }

    };

    RowColumnCallback mNegativeCallback = new RowColumnCallback() {

        @Override
        public void gotoFeedResistanceInput(int row, int column) {
            assertNotEquals("Rows should not be same", mRow, row);
            assertNotEquals("Columns should not be same", mColumns, column);
        }

        @Override
        public void onInvalidInput(String message) {
            assertNotEquals("Error message must not be same", errorMessage, message);
        }

    };
}
