package com.alliance.assignment;

import com.alliance.assignment.model.Extras;
import com.alliance.assignment.presenter.ResistanceTraversalPresenter;
import com.alliance.assignment.utils.OutOfRangeException;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class InitResistanceTest {

	private ResistanceTraversalPresenter rt;

	private final int[][] SAMPLE_DATA = TestData.INPUT_1;
	
	@Before
	public void setUp() throws Exception {
		rt = new ResistanceTraversalPresenter();
	}

	@After
	public void tearDown() throws Exception {
		rt = null;
	}

	@Test
	public void initResistanceMatrixFailedDueToRangeTest() {
		assertNull("Matrix already initialized", rt.getMatrix());
		try {
			rt.initResistanceMatrix(15, 10);
		} catch (OutOfRangeException ore) {
			assertEquals("Failed to throw Out of Range Exception",
					Extras.ROW_RANGE_EXCEPTION, ore.getMessage());
		}
		assertNull("Matrix already initialized", rt.getMatrix());
	}

	@Test
	public void initResistanceMatrixSuccessTest() {
		assertNull("Matrix was initialized", rt.getMatrix());
		try {
			rt.initResistanceMatrix(5, 6);
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		}
		assertNotNull("Matrix initialization failed", rt.getMatrix());
	}

	@Test
	public void feedResistanceMatrixFailureTest() {
		rt.setResistance(null);
		assertNull("Resistance Matrix not Null", rt.getMatrix());
	}

	@Test
	public void feedResistanceMatrixSuccessTest() {
		try {
			rt.initResistanceMatrix(5, 6);
			rt.setResistance(SAMPLE_DATA);
			assertNotNull("Resistance Grid is Null", rt.getMatrix());
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		} catch (ArrayIndexOutOfBoundsException aobe) {
			fail(aobe.getMessage());
		}
	}
	
	@Test
	public void calculateWeightedGridNullTest(){
		boolean flag = false;
		try{
			rt.calculateWeightsToDestination();
		} catch(NullPointerException npe){
			flag = true;
		}
		assertTrue("WeightedMatrix already initialized", flag);
	}

	@Test
	public void calculateWeightedGridSuccessTest(){
		
		try{
			rt.initResistanceMatrix(5, 6);
			rt.setResistance(SAMPLE_DATA);
			rt.calculateWeightsToDestination();
		} catch (OutOfRangeException ore) {
			fail(ore.getMessage());
		} catch (ArrayIndexOutOfBoundsException aobe) {
			fail(aobe.getMessage());
		} catch(NullPointerException npe){
			fail(npe.getMessage());
		}
		int[][] EXPECTED = TestData.WEIGHTED_GRID;
		int[][] actual= rt.getWeightedGrid();
		
		for(int i=0;i<5;i++){
			for(int j=0;j<6;j++){
				assertEquals("Weight not calculated correctly", EXPECTED[i][j] , actual[i][j]);		
			}
		}
	}
}
