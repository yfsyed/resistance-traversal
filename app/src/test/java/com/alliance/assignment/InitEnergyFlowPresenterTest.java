package com.alliance.assignment;

import com.alliance.assignment.listeners.InitEnergyFlowCallback;
import com.alliance.assignment.presenter.InitEnergyFlowPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class InitEnergyFlowPresenterTest {

    private final String expectedError = "Invalid text";

    private int rowCount, initialRow;

    private String errorMessage;

    private InitEnergyFlowPresenter mPresenter;

    @Before
    public void setup() {
        mPresenter = new InitEnergyFlowPresenter();
    }

    @After
    public void tearDown() {
        mPresenter.removeListener();
        mPresenter = null;
    }

    @Test
    public void successfulFragmentVisibleTest() {
        mPresenter.setListener(mPositiveCallback);
        rowCount = 6;
        mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING1, rowCount);
    }

    @Test
    public void failureFragmentVisibleTest() {
        mPresenter.setListener(mNegativeCallback);
        rowCount = 6;
        mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING2, 3);
    }


    @Test
    public void successfulSendActionTest() {
        try {
            mPresenter.setListener(mPositiveCallback);
            rowCount = 6;
            initialRow = 3;
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING1, rowCount);
            mPresenter.onSendAction("" + initialRow);
        } catch (NumberFormatException nfe) {
            fail(nfe.getMessage());
        }
    }

    @Test
    public void failureSendActionTest() {
        try {
            mPresenter.setListener(mNegativeCallback);
            rowCount = 6;
            initialRow = 3;
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING2, 3);
            mPresenter.onSendAction("1");
        } catch (NumberFormatException nfe) {
            fail(nfe.getMessage());
        }
    }

    @Test
    public void successfulSendActionErrorTest() {
        boolean flag = false;
        try {
            mPresenter.setListener(mPositiveCallback);
            rowCount = 6;
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING1, rowCount);
            mPresenter.onSendAction("3-");
        } catch (NumberFormatException nfe) {
            flag = true;
        }
        assertTrue("Should throw Number format exception", flag);
    }

    @Test
    public void failureSendActionErrorTest() {
        boolean flag = false;
        try {
            mPresenter.setListener(mNegativeCallback);
            rowCount = 6;
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING2, 5);
            mPresenter.onSendAction("");
        } catch (NumberFormatException nfe) {
            flag = true;
        }
        assertTrue("Should throw Number format exception", flag);
    }

    @Test
    public void successfulRangeErrorTest() {
        try {
            mPresenter.setListener(mPositiveCallback);
            rowCount = 6;
            initialRow = 5;
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING1, rowCount);
            mPresenter.onSendAction("" + initialRow);
        } catch (NumberFormatException nfe) {
            fail(nfe.getMessage());
        }
    }

    @Test
    public void failureRangeErrorTest() {
        try {
            mPresenter.setListener(mPositiveCallback);
            rowCount = 6;
            initialRow = 0;
            errorMessage = "Row should be in between 1 & 6";
            mPresenter.onFragmentVisible(TestData.TEST_GRID_STRING1, rowCount);
            mPresenter.onSendAction("" + initialRow);
        } catch (NumberFormatException nfe) {
            fail(nfe.getMessage());
        }
    }

    InitEnergyFlowCallback mPositiveCallback = new InitEnergyFlowCallback() {

        @Override
        public void displayResistance(String matrix, int rows) {
            assertEquals("Matrix must be same", TestData.TEST_GRID_STRING1, matrix);
            assertEquals("Row count must be same", rowCount, rows);
        }

        @Override
        public void sendInitialRow(int row) {
            assertEquals("Row must match", initialRow, row);
        }

        @Override
        public void onError(String error) {
            assertEquals("must through error", errorMessage, error);
        }
    };

    InitEnergyFlowCallback mNegativeCallback = new InitEnergyFlowCallback() {

        @Override
        public void displayResistance(String matrix, int rows) {
            assertNotEquals("Matrix must be same", TestData.TEST_GRID_STRING1, matrix);
            assertNotEquals("Row count must be same", rowCount, rows);
        }

        @Override
        public void sendInitialRow(int row) {
            assertNotEquals("Row must match", initialRow, row);
        }

        @Override
        public void onError(String error) {
            assertNotEquals("must through error", errorMessage, error);
        }
    };
}
