package com.alliance.assignment.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.ResistanceFeederInterface;
import com.alliance.assignment.listeners.ResistanceInputListener;
import com.alliance.assignment.presenter.ResistanceInputPresenter;


public class ResistanceFeederFragment extends Fragment
        implements ResistanceInputListener, View.OnClickListener {

    public static final String TAG = ResistanceFeederFragment.class.getSimpleName();

    public ResistanceFeederFragment() {
        // Required empty public constructor
    }

    private TextView mGrid, mGridFeederMessage;

    private Button mAddValue, mSubmit;

    private EditText mGridValue;

    private int mRow, mColumn;

    private ResistanceInputPresenter mPresenter;

    public static ResistanceFeederFragment getInstance(FragmentManager fm, int row, int column) {

        ResistanceFeederFragment fragment = (ResistanceFeederFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new ResistanceFeederFragment();
        }
        Bundle args = new Bundle();
        args.putInt("ROW", row);
        args.putInt("COLUMN", column);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRow = getArguments().getInt("ROW");
            mColumn = getArguments().getInt("COLUMN");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_resistance_feeder, container, false);
        mGridFeederMessage = (TextView) root.findViewById(R.id.grid_feeder_message);
        mGrid = (TextView) root.findViewById(R.id.grid);
        mAddValue = (Button) root.findViewById(R.id.add_resistance_grid);
        mSubmit = (Button) root.findViewById(R.id.button_next);
        mGridValue = (EditText) root.findViewById(R.id.edit_text_grid_value);

        mAddValue.setOnClickListener(this);
        mSubmit.setOnClickListener(this);

        mPresenter = new ResistanceInputPresenter();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mPresenter != null) {
            mPresenter.setListener(this);
            mPresenter.setUpGrid(mRow, mColumn);
        }
    }

    @Override
    public void onPause() {
        if(mPresenter != null) {
            mPresenter.removeListener();
        }
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_next) {
            if (mPresenter != null) {
                mPresenter.onNextClicked();
            }
        } else if (view.getId() == R.id.add_resistance_grid) {
            if (mPresenter != null) {
                String cell = mGridValue.getText().toString().trim();
                mPresenter.addGridCell(cell);
            }
        }
    }

    @Override
    public void updateGridMessage(int row, int column, String grid) {
        mGridFeederMessage.setText(getString(R.string.enter_value, row, column));
        mGrid.setText(grid);
        mGridValue.setText("");
    }

    @Override
    public void sendMatrixForProcessing(int[][] matrix) {
        if (getActivity() != null && getActivity() instanceof ResistanceFeederInterface) {
            ((ResistanceFeederInterface) getActivity()).gotoResults(matrix);
        }
    }

    @Override
    public void disableAddButton() {
        mAddValue.setEnabled(false);
        mGridValue.setEnabled(false);
        mSubmit.setVisibility(View.VISIBLE);
    }

    @Override
    public void invalidInput(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
