package com.alliance.assignment.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.OnEnergyFlowInitListener;
import com.alliance.assignment.listeners.OnResistanceOptionsListener;
import com.alliance.assignment.listeners.ResistanceFeederInterface;
import com.alliance.assignment.listeners.RowColumnsListener;
import com.alliance.assignment.model.Extras;
import com.alliance.assignment.presenter.ResistanceTraversalPresenter;
import com.alliance.assignment.utils.OutOfRangeException;
import com.alliance.assignment.view.fragment.InitEnergyFlowFragment;
import com.alliance.assignment.view.fragment.ResistanceFeederFragment;
import com.alliance.assignment.view.fragment.ResistanceOptions;
import com.alliance.assignment.view.fragment.ResistanceResultsFragment;
import com.alliance.assignment.view.fragment.RowColumnInputFragment;

public class ResistanceTraversalActivity extends AppCompatActivity implements
        OnResistanceOptionsListener, RowColumnsListener,
        OnEnergyFlowInitListener, ResistanceFeederInterface {

    private ResistanceTraversalPresenter mResistancePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resistance_traversal);
        mResistancePresenter = new ResistanceTraversalPresenter();

        if (savedInstanceState == null) {
            addFragment(ResistanceOptions.getInstance(getSupportFragmentManager()));
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void showCustomInput() {
        RowColumnInputFragment fragment = RowColumnInputFragment.getInstance(getSupportFragmentManager());
        replaceFragment(fragment, RowColumnInputFragment.TAG);
    }

    @Override
    public void gotoResistanceFeeder(int row, int column) {
        if (mResistancePresenter != null) {
            try {
                mResistancePresenter.initResistanceMatrix(row, column);
                ResistanceFeederFragment fragment = ResistanceFeederFragment.getInstance(getSupportFragmentManager(), row, column);
                replaceFragment(fragment, ResistanceFeederFragment.TAG);
            } catch (OutOfRangeException ore) {
                Toast.makeText(this, ore.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void showDefaultInput(Extras.DefaultInput type) {
        if (mResistancePresenter != null) {
            try {
                switch (type) {
                    case Success_1:
                        mResistancePresenter.initResistanceMatrix(5, 6);
                        mResistancePresenter.setResistance(Extras.TEST_INPUT_1);
                        break;
                    case Success_2:
                        mResistancePresenter.initResistanceMatrix(5, 6);
                        mResistancePresenter.setResistance(Extras.TEST_INPUT_2);
                        break;
                    case Failure:
                        mResistancePresenter.initResistanceMatrix(3, 5);
                        mResistancePresenter.setResistance(Extras.TEST_INPUT_3);
                        break;
                    default:
                        mResistancePresenter.initResistanceMatrix(5, 6);
                        mResistancePresenter.setResistance(Extras.TEST_INPUT_1);
                        break;
                }
                mResistancePresenter.calculateWeightsToDestination();
                gotoInitEnergyFlow();
            } catch (OutOfRangeException ore) {
                Toast.makeText(this, ore.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public int getMatrixRows() {
        if (mResistancePresenter != null) {
            mResistancePresenter.getRowCount();
        }
        return 1;
    }

    @Override
    public void gotoResults(int[][] matrix) {
        if (mResistancePresenter != null) {
            mResistancePresenter.setResistance(matrix);
            mResistancePresenter.calculateWeightsToDestination();
            gotoInitEnergyFlow();
        }
    }

    @Override
    public void evaluateAndShowResults(int entryRow) {
        if (mResistancePresenter != null) {
            mResistancePresenter.evaluateResistance(entryRow);
            gotoResistanceResults();
        }
    }

    @Override
    public String getResistanceMatrix() {
        if (mResistancePresenter != null) {
            return mResistancePresenter.getMatrixString();
        }
        return "";
    }

    public void gotoInitEnergyFlow() {
        InitEnergyFlowFragment fragment = InitEnergyFlowFragment.getInstance(getSupportFragmentManager());
        replaceFragment(fragment, InitEnergyFlowFragment.TAG);
    }

    public void gotoResistanceResults() {
        if (mResistancePresenter != null) {
            ResistanceResultsFragment fragment = ResistanceResultsFragment.getInstance(getSupportFragmentManager(),
                    mResistancePresenter.getMatrixString(),
                    mResistancePresenter.getEnergyReleased(),
                    mResistancePresenter.getTotalResistance(),
                    mResistancePresenter.getTraversedRows());
            replaceFragment(fragment, ResistanceResultsFragment.TAG);
        }
    }

    private void replaceFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment, tag).addToBackStack(tag).commit();

    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment, ResistanceOptions.TAG)
                .addToBackStack(ResistanceOptions.TAG)
                .commit();
    }

}
