package com.alliance.assignment.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.RowColumnCallback;
import com.alliance.assignment.listeners.RowColumnsListener;
import com.alliance.assignment.presenter.RowColumnInputPresenter;

public class RowColumnInputFragment extends Fragment implements
        RowColumnCallback, View.OnClickListener, TextView.OnEditorActionListener {

    public static final String TAG = RowColumnInputFragment.class.getSimpleName();

    private RowColumnInputPresenter mPresenter;

    private EditText mRowField, mColumnField;

    public RowColumnInputFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlankFragment.
     */
    public static RowColumnInputFragment getInstance(FragmentManager fm) {
        RowColumnInputFragment fragment = (RowColumnInputFragment) fm.findFragmentByTag(TAG);
        if(fragment == null) {
         fragment = new RowColumnInputFragment();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_row_column_input, container, false);

        mRowField = (EditText) root.findViewById(R.id.edit_text_row);
        mColumnField = (EditText) root.findViewById(R.id.edit_text_column);
        mColumnField.setOnEditorActionListener(this);
        root.findViewById(R.id.button_next).setOnClickListener(this);
        mPresenter = new RowColumnInputPresenter();
        return root;
    }


    @Override
    public void onResume(){
        super.onResume();
        if(mPresenter != null) {
            mPresenter.setListener(this);
        }
    }

    @Override
    public void onPause(){
        if(mPresenter != null) {
            mPresenter.removeListener();
        }
        super.onPause();
    }

    @Override
    public void onClick(View view){
        if(view.getId() == R.id.button_next){
            onNextClicked();
        }
    }


    public void onNextClicked(){
        String row = mRowField.getText().toString().trim();
        String column = mColumnField.getText().toString().trim();
        if(mPresenter != null) {
            mPresenter.sendRowColumn(row, column);
        }
    }

    @Override
    public void gotoFeedResistanceInput(int row, int column){
        if(getActivity() != null && getActivity() instanceof RowColumnsListener) {
            ((RowColumnsListener)getActivity()).gotoResistanceFeeder(row, column);
        }
    }

    @Override
    public void onInvalidInput(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if(actionId == EditorInfo.IME_ACTION_NEXT){
            onNextClicked();
            return true;
        }
        return false;
    }
}
