package com.alliance.assignment.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.OnResultsCallback;
import com.alliance.assignment.presenter.ResistanceResultPresenter;

public class ResistanceResultsFragment extends Fragment implements OnResultsCallback {

    public static final String TAG = ResistanceResultsFragment.class.getSimpleName();

    private static final String GRID = "grid";
    private static final String RESULT = "result";
    private static final String TOTAL_RESISTANCE = "resistance";
    private static final String PATH = "path";

    private String mGrid;
    private String mResult;
    private int mResistance;
    private String mPath;

    private TextView mGridView, mResultView, mTotalResistance, mPathView;

    private ResistanceResultPresenter mPresenter;

    public ResistanceResultsFragment() {
        // Required empty public constructor
    }

    public static ResistanceResultsFragment getInstance(
            FragmentManager fm, String param1, String param2, int param3, String param4) {
        ResistanceResultsFragment fragment = (ResistanceResultsFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new ResistanceResultsFragment();
        }

        Bundle args = new Bundle();
        args.putString(GRID, param1);
        args.putString(RESULT, param2);
        args.putInt(TOTAL_RESISTANCE, param3);
        args.putString(PATH, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGrid = getArguments().getString(GRID);
            mResult = getArguments().getString(RESULT);
            mResistance = getArguments().getInt(TOTAL_RESISTANCE);
            mPath = getArguments().getString(PATH);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_resistance_results, container, false);
        mGridView = (TextView) root.findViewById(R.id.grid);
        mResultView = (TextView) root.findViewById(R.id.result);
        mTotalResistance = (TextView) root.findViewById(R.id.total_resistance);
        mPathView = (TextView) root.findViewById(R.id.path);
        mPresenter = new ResistanceResultPresenter();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.setListener(this);
        mPresenter.onResistanceResults(mGrid, mResult, mResistance, mPath);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updatePath(String path) {
        mPathView.setText(getString(R.string.path_traversed, path));
    }

    @Override
    public void updateTotalResistance(int resistance) {
        mTotalResistance.setText(getString(R.string.total_resistance, resistance));
    }

    @Override
    public void updateGrid(String grid) {
        mGridView.setText(grid);
    }

    @Override
    public void updateResult(String result) {
        mResultView.setText(getString(R.string.energy_released, result));
    }


}
