package com.alliance.assignment.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.OnResistanceOptionsListener;
import com.alliance.assignment.model.Extras;

public class ResistanceOptions extends Fragment implements View.OnClickListener{

    public static final String TAG = ResistanceOptions.class.getSimpleName();

    public ResistanceOptions() {
        // Required empty public constructor
    }

    public static ResistanceOptions getInstance(FragmentManager fm) {
        ResistanceOptions fragment = (ResistanceOptions)fm.findFragmentByTag(TAG);
        if(fragment == null){
            fragment = new ResistanceOptions();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_resistance_options, container, false);
        root.findViewById(R.id.custom_input).setOnClickListener(this);
        root.findViewById(R.id.default_input_1).setOnClickListener(this);
        root.findViewById(R.id.default_input_2).setOnClickListener(this);
        root.findViewById(R.id.default_input_3).setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.default_input_1){
            showDefaultInputs(Extras.DefaultInput.Success_1);
        } else if(view.getId() == R.id.default_input_2){
            showDefaultInputs(Extras.DefaultInput.Success_2);
        } else if(view.getId() == R.id.default_input_3){
            showDefaultInputs(Extras.DefaultInput.Failure);
        } else if(view.getId() == R.id.custom_input){
            showCustomInputs();
        }
    }

    private void showCustomInputs(){
        if(getActivity() != null && getActivity() instanceof OnResistanceOptionsListener){
            ((OnResistanceOptionsListener) getActivity()).showCustomInput();
        }
    }

    private void showDefaultInputs(Extras.DefaultInput type){
        if(getActivity() != null && getActivity() instanceof OnResistanceOptionsListener){
            ((OnResistanceOptionsListener) getActivity()).showDefaultInput(type);
        }
    }

}
