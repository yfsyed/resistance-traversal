package com.alliance.assignment.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alliance.assignment.R;
import com.alliance.assignment.listeners.InitEnergyFlowCallback;
import com.alliance.assignment.listeners.OnEnergyFlowInitListener;
import com.alliance.assignment.presenter.InitEnergyFlowPresenter;

public class InitEnergyFlowFragment extends Fragment implements
        View.OnClickListener, InitEnergyFlowCallback {

    public static final String TAG = InitEnergyFlowFragment.class.getSimpleName();

    private EditText mRowNumber;

    private TextView mResistanceGrid, mRowMessage;

    private OnEnergyFlowInitListener mListener;

    private InitEnergyFlowPresenter mPresenter;

    public InitEnergyFlowFragment() {
        // Required empty public constructor
    }


    public static InitEnergyFlowFragment getInstance(FragmentManager fm) {
        InitEnergyFlowFragment fragment = (InitEnergyFlowFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new InitEnergyFlowFragment();
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_init_energy_flow, container, false);
        mRowMessage = (TextView) root.findViewById(R.id.init_energy_message);
        mResistanceGrid = (TextView) root.findViewById(R.id.resistance_grid);
        final Button mSend = (Button) root.findViewById(R.id.button_init_energy_flow);
        mRowNumber = (EditText) root.findViewById(R.id.edit_text_initial_row);
        mSend.setOnClickListener(this);
        mPresenter = new InitEnergyFlowPresenter();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.setListener(this);
        if (getActivity() instanceof OnEnergyFlowInitListener && mPresenter != null) {
            mListener = (OnEnergyFlowInitListener) getActivity();
            if (mListener != null) {
                int rows = mListener.getMatrixRows();
                String resistanceMatrix = mListener.getResistanceMatrix();
                mPresenter.onFragmentVisible(resistanceMatrix, rows);
            }
        } else {
            throw new RuntimeException("Activity must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mListener = null;
        if (mPresenter != null) {
            mPresenter.removeListener();
        }
    }

    @Override
    public void displayResistance(String matrix, int rows) {
        mRowMessage.setText(getString(R.string.init_energy_message, rows));
        mResistanceGrid.setText(matrix);
    }

    @Override
    public void sendInitialRow(int row) {
        if (mListener != null) {
            mListener.evaluateAndShowResults(row);
        }
    }

    @Override
    public void onError(String error) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (mPresenter != null) {
            String rowNumber = mRowNumber.getText().toString().trim();
            mPresenter.onSendAction(rowNumber);
        }
    }
}
