package com.alliance.assignment.model;

public class Extras {

	public static final String ROW_RANGE_EXCEPTION = "Row out of range";
	
	public static final String COLUMN_RANGE_EXCEPTION = "Column out of range";
	
	public enum DefaultInput {
		Success_1,
		Success_2,
		Failure
	}

	public static final int TEST_INPUT_1[][] = { { 3, 4, 1, 2, 8, 6 },
			{ 6, 1, 8, 2, 7, 4 }, { 5, 9, 3, 9, 9, 5 }, { 8, 4, 1, 3, 2, 6 },
			{ 3, 7, 2, 8, 6, 4 } };

	public static final int TEST_INPUT_2[][] = { { 3, 4, 1, 2, 8, 6 },
			{ 6, 1, 8, 2, 7, 4 }, { 5, 9, 3, 9, 9, 5 }, { 8, 4, 1, 3, 2, 6 },
			{ 3, 7, 2, 1, 2, 3 } };

	public static final int TEST_INPUT_3[][] = { { 19, 10, 19, 10, 19 },
			{ 21, 23, 20, 19, 12 }, { 20, 12, 20, 11, 10 } };
}
