package com.alliance.assignment.presenter;

import android.text.TextUtils;

import com.alliance.assignment.listeners.ResistanceInputListener;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class ResistanceInputPresenter {

    public ResistanceInputListener getListener() {
        return mListener;
    }

    public void setListener(ResistanceInputListener listener) {
        mListener = listener;
    }

    public void removeListener() {
        mListener = null;
    }

    private ResistanceInputListener mListener;

    private int mColumn;

    private int mRow;

    private int column = 0;

    private int row = 0;

    private int[][] matrix;

    private StringBuilder sb;

    public void setUpGrid(int rows, int columns) {
        mRow = rows;
        mColumn = columns;
        matrix = new int[mRow][mColumn];
        sb = new StringBuilder();
        updateMessages();
    }


    public void addGridCell(String cell) {
        try {
            if (!TextUtils.isEmpty(cell)) {
                int value = Integer.parseInt(cell);
                matrix[row][column] = value;
                sb.append(value + "  ");
                if (column < mColumn) {
                    column++;
                }
                updateMessages();
                if (column == mColumn) {
                    if (row == mRow - 1) {
                        sendDisableButton();
                    } else {
                        column = 0;
                        row++;
                        sb.append("\n");
                    }
                }
            } else {
                showError("Invalid input");
            }
        } catch (NumberFormatException nfe) {
            showError(nfe.getMessage());
        }
    }

    public void onNextClicked() {
        gotoResults();
    }

    private void gotoResults() {
        if (mListener != null) {
            mListener.sendMatrixForProcessing(matrix);
        }
    }

    private void updateMessages() {
        if (mListener != null) {
            mListener.updateGridMessage(row, column, sb.toString());
        }
    }


    private void sendDisableButton() {
        if (mListener != null) {
            mListener.disableAddButton();
        }
    }

    private void showError(String error) {
        if (mListener != null) {
            mListener.invalidInput(error);
        }
    }
}
