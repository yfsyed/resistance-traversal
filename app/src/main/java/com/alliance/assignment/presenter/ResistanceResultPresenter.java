package com.alliance.assignment.presenter;

import com.alliance.assignment.listeners.OnResultsCallback;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class ResistanceResultPresenter {

    public OnResultsCallback mListener;

    public OnResultsCallback getListener() {
        return mListener;
    }

    public void setListener(OnResultsCallback listener) {
        mListener = listener;
    }

    public void removeListener(){
        mListener = null;
    }

    public void onResistanceResults(String grid, String result, int totalResistance, String path) {
        updateTotalResistance(totalResistance);
        updatePath(path);
        updateGrid(grid);
        updateResult(result);
    }

    private void updatePath(String path) {
        if (mListener != null) {
            mListener.updatePath(path);
        }
    }

    private void updateTotalResistance(int resistance) {
        if (mListener != null) {
            mListener.updateTotalResistance(resistance);
        }
    }

    private void updateGrid(String grid) {
        if (mListener != null) {
            mListener.updateGrid(grid);
        }
    }

    private void updateResult(String result) {
        if (mListener != null) {
            mListener.updateResult(result);
        }
    }
}
