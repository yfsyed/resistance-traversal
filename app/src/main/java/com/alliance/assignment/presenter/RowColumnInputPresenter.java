package com.alliance.assignment.presenter;

import android.text.TextUtils;

import com.alliance.assignment.listeners.RowColumnCallback;
import com.alliance.assignment.utils.OutOfRangeException;
import com.alliance.assignment.utils.ResistanceTraversalHelper;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class RowColumnInputPresenter {

    private RowColumnCallback mListener;

    public RowColumnCallback getListener() {
        return mListener;
    }

    public void setListener(RowColumnCallback mListener) {
        this.mListener = mListener;
    }

    public void removeListener() {
        this.mListener = null;
    }

    public void sendRowColumn(String rowString, String columnString){
        if(TextUtils.isEmpty(rowString) || TextUtils.isEmpty(columnString)){
           sendError("Invalid Inputs");
        } else {
            try{
                int row = Integer.parseInt(rowString);
                int column = Integer.parseInt(columnString);
                ResistanceTraversalHelper.validateMatrixRange(row, column);
                sendInput(row, column);
            } catch(NumberFormatException nfe){
                sendError(nfe.getLocalizedMessage());
            } catch(OutOfRangeException ore){
                sendError(ore.getMessage());
            }
        }
    }

    private void sendError(String message){
        if(mListener != null){
            mListener.onInvalidInput(message);
        }
    }

    private void sendInput(int row, int column){
        if(mListener != null){
            mListener.gotoFeedResistanceInput(row, column);
        }
    }
}
