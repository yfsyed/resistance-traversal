package com.alliance.assignment.presenter;

import com.alliance.assignment.listeners.InitEnergyFlowCallback;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public class InitEnergyFlowPresenter {

    private InitEnergyFlowCallback mListener;

    private String mMatrix;

    private int mRows;

    public void setListener(InitEnergyFlowCallback listener) {
        mListener = listener;
    }

    public void removeListener() {
        mListener = null;
    }

    public void onFragmentVisible(String matrix, int row) {
        mMatrix = matrix;
        mRows = row;
        displayGrid(matrix, row);
    }

    public void onSendAction(String rowNumber) throws NumberFormatException {
            int row = Integer.parseInt(rowNumber);
            if (row < 1 || row > mRows) {
                showError("Row should be in between 1 & " + mRows);
            } else {
                sendRow(row);
            }
    }

    private void displayGrid(String matrix, int rows) {
        if (mListener != null) {
            mListener.displayResistance(matrix, rows);
        }
    }

    private void sendRow(int row) {
        if (mListener != null) {
            mListener.sendInitialRow(row);
        }
    }

    private void showError(String error) {
        if (mListener != null) {
            mListener.onError(error);
        }
    }

}
