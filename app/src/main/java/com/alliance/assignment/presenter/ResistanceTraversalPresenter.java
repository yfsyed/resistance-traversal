package com.alliance.assignment.presenter;

import com.alliance.assignment.utils.OutOfRangeException;
import com.alliance.assignment.utils.ResistanceTraversalHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ResistanceTraversalPresenter {

    private final int RESISTANCE_LIMIT = 50;

    private int[][] resistanceMatrix = null;

    private int[][] weightedMatrix = null;

    private int column = 5;

    private int row = 1;

    private String energyReleased = "NO";

    private int totalResistance = 0;

    private List<Integer> traversedRows = new ArrayList<Integer>();

    private String resistanceGridAsString = "";

    /**
     * Using default values for the generator.
     */
    public void feedResistanceMatrix() {
        Scanner in = new Scanner(System.in);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < row; i++) {
            System.out.println("Enter next row values");
            for (int j = 0; j < column; j++) {
                System.out.print("Enter next column value ");
                resistanceMatrix[i][j] = in.nextInt();
                sb.append(resistanceMatrix[i][j]);
                sb.append("  ");
            }
            sb.append("\n");
        }

        resistanceGridAsString = sb.toString();
    }

    /**
     * @param i row
     * @param j column
     */
    public void initResistanceMatrix(int i, int j) throws OutOfRangeException {
        ResistanceTraversalHelper.validateMatrixRange(i, j);
        resistanceMatrix = new int[i][j];
        weightedMatrix = new int[i][j];
        traversedRows = new ArrayList<Integer>();
        row = i;
        column = j;
    }

    /**
     * Calculate weights to reach the destination from a given cell.
     */
    public void calculateWeightsToDestination() throws NullPointerException {
        int lastColumn = column - 1;
        int horizontal, up, down;

        if (weightedMatrix == null || resistanceMatrix == null) {
            throw new NullPointerException();
        }

        for (int j = lastColumn; j >= 0; j--) {
            for (int i = 0; i < row; i++) {
                if (j == lastColumn) {
                    weightedMatrix[i][j] = resistanceMatrix[i][j];
                } else {

                    int upRow = ResistanceTraversalHelper.getRow(i - 1, row);
                    int downRow = ResistanceTraversalHelper.getRow(i + 1, row);

                    horizontal = weightedMatrix[i][j + 1];
                    up = weightedMatrix[upRow][j + 1];
                    down = weightedMatrix[downRow][j + 1];

                    if (horizontal <= up && horizontal <= down) {
                        weightedMatrix[i][j] = getResistanceAt(i, j) + horizontal;
                    } else if (up < horizontal && up < down) {
                        weightedMatrix[i][j] = getResistanceAt(i, j) + up;
                    } else {
                        weightedMatrix[i][j] = getResistanceAt(i, j) + down;
                    }
                }
            }
        }
    }

    /**
     * Evaluate least resistance path from a given cell.
     *
     * @param entry
     */
    public void evaluateResistance(int entry) {
        int i = entry - 1;
        int j = 0;

        int horizontal, up, down;
        boolean terminated = false;

        totalResistance = getResistanceAt(i, j);
        traversedRows.clear();
        traversedRows.add(i + 1);


        while (j < column - 1) {

            int upRow = ResistanceTraversalHelper.getRow(i - 1, row);
            int downRow = ResistanceTraversalHelper.getRow(i + 1, row);

            horizontal = weightedMatrix[i][j + 1];
            up = weightedMatrix[upRow][j + 1];
            down = weightedMatrix[downRow][j + 1];

            int temp = 0;
            if (horizontal <= up && horizontal <= down) {
                temp = getResistanceAt(i, j + 1);
            } else if (up < horizontal && up < down) {
                temp = getResistanceAt(upRow, j + 1);
                i = upRow;
            } else {
                temp = getResistanceAt(downRow, j + 1);
                i = downRow;
            }

            if (totalResistance + temp < RESISTANCE_LIMIT) {
                totalResistance += temp;
                traversedRows.add(i + 1);
                j++;
            } else {
                terminated = true;
                break;
            }
        }

        setEnergyReleased(terminated ? "NO" : "YES");

    }

    private void printResults() {
        System.out.println(getEnergyReleased());
        System.out.println(getTotalResistance());
        System.out.println(traversedRows.toString());
    }

    /**
     * Get ResistanceMatrix
     *
     * @return
     */
    public int[][] getMatrix() {
        return resistanceMatrix;
    }

    /**
     * To set the resistance to the grid.
     *
     * @param resistance
     */
    public void setResistance(int[][] resistance) {
        resistanceMatrix = resistance;
        printGrid(resistanceMatrix);
    }

    public String getMatrixString() {
        return resistanceGridAsString;
    }


    /**
     * To get the resistance at a particular location.
     *
     * @param row
     * @param column
     * @return
     */
    public int getResistanceAt(int row, int column) throws NullPointerException {
        return resistanceMatrix[row][column];
    }

    /**
     * To get the weighted Grid that holds the
     * shortest resistance from a give node to destination.
     *
     * @return
     */
    public int[][] getWeightedGrid() {
        return weightedMatrix;
    }

    public String getEnergyReleased() {
        return energyReleased;
    }

    public void setEnergyReleased(String energyReleased) {
        this.energyReleased = energyReleased;
    }

    public int getTotalResistance() {
        return totalResistance;
    }

    public void setTotalResistance(int totalResistance) {
        this.totalResistance = totalResistance;
    }

    public String getTraversedRows() {
        if (traversedRows != null) {
            return traversedRows.toString();
        }
        return "";
    }

    public void setTraversedRows(List<Integer> traversedRows) {
        this.traversedRows = traversedRows;
    }

    public int getRowCount() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    /**
     * To print grid data.
     *
     * @param grid
     */
    public void printGrid(int[][] grid) {
        StringBuilder sb = new StringBuilder();
        if(grid != null) {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < column; j++) {
                    System.out.print(grid[i][j] + " ");
                    sb.append(grid[i][j] + "  ");
                }
                System.out.println();
                sb.append("\n");
            }
        }
        resistanceGridAsString = sb.toString();
    }
}
