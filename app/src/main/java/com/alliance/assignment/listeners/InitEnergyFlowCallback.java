package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface InitEnergyFlowCallback {

    void displayResistance(String matrix, int rows);

    void sendInitialRow(int row);

    void onError(String error);
}
