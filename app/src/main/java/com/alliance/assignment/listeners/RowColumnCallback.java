package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface RowColumnCallback {

    public void gotoFeedResistanceInput(int row, int column);

    public void onInvalidInput(String message);
}
