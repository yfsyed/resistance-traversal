package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface RowColumnsListener {

    void gotoResistanceFeeder(int row, int column);

}
