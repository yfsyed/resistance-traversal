package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface OnResultsCallback {

    public void updatePath(String path);

    public void updateTotalResistance(int resistance);

    public void updateGrid(String grid);

    public void updateResult(String result);
}
