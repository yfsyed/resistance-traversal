package com.alliance.assignment.listeners;

import com.alliance.assignment.model.Extras;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface OnResistanceOptionsListener {

    void showCustomInput();

    void showDefaultInput(Extras.DefaultInput type);

}