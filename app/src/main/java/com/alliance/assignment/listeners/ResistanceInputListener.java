package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface ResistanceInputListener {

    void updateGridMessage(int row, int column, String message);

    void sendMatrixForProcessing(int[][] matrix);

    void disableAddButton();

    void invalidInput(String message);
}