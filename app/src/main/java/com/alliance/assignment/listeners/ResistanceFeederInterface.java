package com.alliance.assignment.listeners;

/**
 * Created by Yousuf Syed on 7/6/16.
 */
public interface ResistanceFeederInterface {
    void gotoResults(int[][] matrix);
}
