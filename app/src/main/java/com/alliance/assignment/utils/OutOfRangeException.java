package com.alliance.assignment.utils;

@SuppressWarnings("serial")
public class OutOfRangeException extends Exception{
	
	public OutOfRangeException(String message){
		super(message);
	}

}
