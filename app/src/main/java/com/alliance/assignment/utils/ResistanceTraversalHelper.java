package com.alliance.assignment.utils;


import com.alliance.assignment.model.Extras;

public class ResistanceTraversalHelper {
	
	
	/**
	 * To Validate the row and column range.
	 * 
	 * @param row 					no of rows for the matrix
	 * @param column				no of columns for the matrix
	 * @throws OutOfRangeException
	 * 								1 <= row <= 10 		valid
	 * 								5 <= column <= 100  valid 
	 * 								else throws exception.
	 */
	public static void validateMatrixRange(int row, int column) throws OutOfRangeException {
		if(row < 1 || row > 10) {
			throw new OutOfRangeException(Extras.ROW_RANGE_EXCEPTION);
		} else if (column < 5 || column > 100){
			throw new OutOfRangeException(Extras.COLUMN_RANGE_EXCEPTION);
		}
	}
	
	/**
	 * Get row based on how energy flows up or down.
	 * 
	 * @param i   current row index.
	 * @param row last row index
	 * @return    next row index
	 */
	public static int getRow(int i, int row){
			if (i == -1) {
				i = row - 1;
			} else if (i == row) {
				i = 0;
			}
			return i;
	}

}
